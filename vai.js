    function removeClass(object, classStr){
      object.className = object.className.replace( new RegExp("\\b" + classStr + "\\b"), "")
    }
    
    function findParentWithClass(element, cName){
      while(element != window && element.className.match( new RegExp(cName) )==null ){
        element = element.parentNode
      }  
      
      return element
    }

    
    document.addEventListener("DOMContentLoaded", function(){
    
      document.getElementById('createCharButton').addEventListener('click', function(){
        var c = new Char()
        c.appendToList()
      })
      


    })
    
    function sortChars(){
      var charList = document.getElementById("charList")
      var charElements = charList.querySelectorAll(".playerSheet")
      return
    }
        
    function sortCharsByInitiative(roll_dice){
      var charList = document.getElementById("charList")
      
      var playerSheets = charList.querySelectorAll('.playerSheet')
      var playerSheetArray = []
      for(pi=0; pi<playerSheets.length; pi++){
        var player = charList.removeChild(playerSheets[pi])

        var playerBaseInit = parseInt(player.querySelector('[name=baseÌnitiativeScore]').value)
        
        var dice_num_faces = parseInt(document.getElementById("dieType").value)
        var player_roll_value = parseInt(player.querySelector('[name=initiativeRoll]').value) || ""
        if(player_roll_value == "" || roll_dice){
          player_roll_value = parseInt(Math.random() * dice_num_faces + 1)
          console.log("rolled a " + player_roll_value)

          player.querySelector('[name=initiativeRoll]').value = player_roll_value
        }
        
        var child_penalty = parseInt(player.querySelector('[name=penalty]').value)
       
        player.querySelector('[name=totalInitiativeScore]').value = "" +
                                                                    (playerBaseInit +
                                                                     player_roll_value +
                                                                     child_penalty)
        playerSheetArray.push(player)
      }

      playerSheetArray.sort(function(c1, c2){
        var c1_score = parseInt(c1.querySelector('[name=totalInitiativeScore]').value)
        var c2_score = parseInt(c2.querySelector('[name=totalInitiativeScore]').value)
        
        return c1_score - c2_score 
      })

      while(playerSheetArray.length > 0){
        charList.appendChild(playerSheetArray.pop())
      }
      
    }

    function generatePenaltyAccounter(healthContainer, penaltyDisplay){
      var f = function(e){
        if(e.target.tagName.toLowerCase() != "input" )
          return
        
        var checkboxes = healthContainer.querySelectorAll('input[type=checkbox]')
        
        if(e.target.checked){
          for (var i = 0; i < checkboxes.length; i++){
            if(e.target == checkboxes[i])
              break
            
            checkboxes[i].checked = true
          }
          penaltyDisplay.value = e.target.dataset.penalty
        }else{
          var previousPenalty = 0
          for (var i = 0; i < checkboxes.length; i++){
            if(e.target == checkboxes[i]){
              for(i++; i < checkboxes.length; i++){
                checkboxes[i].checked = false
              }
              break
            }
            previousPenalty = checkboxes[i].dataset.penalty
          }
          penaltyDisplay.value = previousPenalty
        }
        //sortCharsByInitiative(true)
      }
      return f
    }

    function Char(base_score, char_name){
      this.template = document.getElementById("charTemplate").cloneNode(true)
      this.template.style.display = "block"
      this.template.id = ""
      this.template.style = "background-color: rgb(" + parseInt(Math.random() * 255) + ", " +
                                                      + parseInt(Math.random() * 255) + ", " +
                                                      + parseInt(Math.random() * 255) + ");"
      
      this.healthContainer = this.template.querySelector('fieldset[name=healthContainer]')
      this.charNameDisplay = this.template.querySelector('input[name=charName]')
      if(char_name == undefined){
        char_name = "Monster"
      }
      this.charNameDisplay.value  = char_name
      this.baseInitDisplay= this.template.querySelector('input[name=baseÌnitiativeScore]')
      if(base_score != undefined){
        this.baseInitDisplay.value = base_score
      }else{
        this.baseInitDisplay.value = 1
      }
      this.penaltyDisplay = this.template.querySelector('input[name=penalty]')
      this.healthContainer.addEventListener('click', generatePenaltyAccounter(this.healthContainer,this.penaltyDisplay) )
      this.closeButton = this.template.querySelector('.deleteCharButton')
      var charSheet = this.template
      var penaltyDisplay = this.penaltyDisplay
      this.closeButton.addEventListener('click', function(e){
        charSheet.parentNode.removeChild(charSheet)
      })

      this.killButton = this.template.querySelector('.killCharButton')
      this.killButton.addEventListener('click', function(e){
        var checkboxes = charSheet.querySelectorAll('input[type=checkbox]')
        if(charSheet.style.opacity == '0.3'){
          e.target.innerHTML = '☠ kill'
          charSheet.style.opacity = '1.0'
          for(var i=0; i<checkboxes.length; i++){
            checkboxes[i].checked = ''
          }
          penaltyDisplay.value = '0'
        }else{
          e.target.innerHTML = '☥ unkill'
          charSheet.style.opacity = '0.3'
          for(var i=0; i<checkboxes.length; i++){
            checkboxes[i].checked = 'checked'
          }
          penaltyDisplay.value = '-99'
        }
        //sortChars()
      })
    }

    Char.prototype.appendToList = function(){
      var charList = document.getElementById("charList")
      document.getElementById("charList").appendChild(this.template)
      //~ var percentWidth = (100 / charList.children.length) + "%"
      //~ for (var i = 0; i < charList.children.length; i++){
        //~ charList.children[i].style.width= percentWidth
      //~ }
      
    }

    Char.prototype.setAttribute = function(attr_name, value){
      this.template.querySelector("[name=" + attr_name + "]").value = value
    }

    Char.prototype.getAttribute = function(attr_name){
      return this.template.querySelector("[name=" + attr_name + "]").value
    }
    

